# ESolutions

## Overview
ESolutions the word was write from `Ecommerce Solutions`.\
It small project help client easy setup and solve business problem.\
The client can sell anything through space, time.


## Architecture Overview
The web application cross-platform at the server and client side.
The service can running on Linux, Window or MacOs depending on your Docker.\
The architecture propose a microservice implement clean achitecture with behavior pattern ("Mediator"). Use RabbitMQ for message broker so light message

<img src="https://user-images.githubusercontent.com/31025072/82693606-06b37480-9c8c-11ea-9d76-7aa71853835e.jpg" width="100%" />